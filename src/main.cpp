/**      +--------------+  +-----------+
 *       | myfirstactor |  | diceroller|
 *       +--------------+  +-----------+
 * --start-->   |                 |
 *              |   -- *spawn* -> |
 *              |  ---- roll -->  |
 *              |  <---  # -----  |
 *              |  ---- roll -->  |
 *              |  <---  # -----  |
 *              |                 |
 */

#include "prelude.hpp"

using start = atom_constant<atom("start")>;
using roll  = atom_constant<atom("roll")>;

behavior diceroller(event_based_actor *self) {
    behavior fast{
        [=](roll, actor &sender) {
            aout(self) << "Rolling really fast, hold on!" << endl;
            self->send(sender, 3);
        }
    };
    behavior slow{
        [=](roll, actor &sender) {
            aout(self) << "Rolling, hold on!" << endl;
            expensive_compute();
            self->send(sender, 4);
            self->become(fast);
        }
    };
    return slow;
}

behavior myfirstactor(event_based_actor *self) {
    return {
        [=](start) {
            aout(self) << "Started!" << endl;
            //auto dr = spawn(diceroller);
            auto dr = io::remote_actor("localhost", 10000);
            self->send(dr, roll::value, self);
            self->send(dr, roll::value, self);
            self->send(dr, roll::value, self);
        },
        [=](int value) {
            aout(self) << "got value: " << value << endl;
        },
    };
}

int main()
{
    scoped_actor s;

    auto mfa = spawn(myfirstactor);
    s->send(mfa, start::value);

//    auto dr = spawn(diceroller);
//    io::publish(dr, 10000, nullptr, true);

    s->await_all_other_actors_done();
}

