## Quick start

- `git submodule update --init --recursive`
- `cmake .`
- `make -j 8`

### Compile CAF library

- `cd libs/caf`
- `./configure`
- `make`
- `sudo make install`

## Slides

[http://cppse.nl/tdcug/actor-model.php][1]

[1]: http://cppse.nl/tdcug/actor-model.php

