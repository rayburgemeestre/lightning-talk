#pragma once

#include "caf/all.hpp"
#include "caf/io/all.hpp"

using std::endl;
using namespace caf;

auto nothing = [](){};

#include <experimental/optional>

namespace std {
    using std::experimental::optional;
    using std::experimental::make_optional;
}

using namespace std;

void expensive_compute() {
    volatile int c = 0;
    for (unsigned long i=0; i<1000000000; i++) {
        c++;
    }
}

